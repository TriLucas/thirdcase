# -*- coding: utf-8 -*-

from django import forms


class UserForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(),
        label=u'Nutzername',
        required=False,
    )
    password = forms.CharField(
        widget=forms.PasswordInput(),
        label=u'Passwort',
        required=False,
    )
