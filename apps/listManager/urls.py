# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from apps.listManager import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^login/', views.sitelogin, name='login'),
    url(r'^logout/', views.sitelogout, name='logout'),
)
