# -*- coding: utf-8 -*-

from django.shortcuts import redirect
from libs.jinja import render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse_lazy

from apps.songlist.models import Song
from apps.songlist.models import Set
from apps.listManager.forms import UserForm


def sitelogin(request):
    context = {}

    if request.method == 'POST':
        post_data = request.POST
    else:
        post_data = None

    the_form = UserForm(data=post_data)

    if request.method == 'POST' and the_form.is_valid():
        user = authenticate(
            username=the_form.cleaned_data.get('username'),
            password=the_form.cleaned_data.get('password'),
        )
        if user is not None:
            login(request, user)
            return redirect(request.GET['next'])

        the_form.add_error(None, u'Ungültige Login-Daten!')

    context.update(csrf(request))
    context['form'] = the_form
    return render_to_response('main/inc_login.html', context)


@login_required(login_url=reverse_lazy('login'))
def sitelogout(request):
    logout(request)
    return redirect(index)


@login_required(login_url=reverse_lazy('login'))
def index(request):
    context = {}
    number_songs = Song.actual.count()
    number_sets = Set.objects.count()
    number_sugs = Song.suggestions.count()

    context.update(csrf(request))
    context['active'] = 'index'
    context['n_songs'] = number_songs
    context['n_sets'] = number_sets
    context['n_sugs'] = number_sugs
    return render_to_response('main/inc_index.html', context)
