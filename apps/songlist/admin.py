from django.contrib import admin
from apps.songlist.models import Song
from apps.songlist.models import Set
from apps.songlist.models import PdfErrorLog


class SongAdmin(admin.ModelAdmin):
    list_filter = ('is_suggestion', 'available', 'editor')


class SetAdmin(admin.ModelAdmin):
    list_filter = ('author', 'editor')


class PdfErrorLogAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

admin.site.register(Song, SongAdmin)
admin.site.register(Set, SetAdmin)
admin.site.register(PdfErrorLog, PdfErrorLogAdmin)

# Register your models here.
