# -*- coding: utf-8 -*-
from django.core.context_processors import csrf
from django.forms import ModelForm
from django.db import IntegrityError
from django import forms

from datetime import datetime

from apps.songlist.models import Song
from apps.songlist.models import Set

from apps.thirdcasler.models import Casler

from libs.jinja import render_to_string


class SongForm(ModelForm):
    approve = forms.BooleanField(
        label=u'Me gusta!',
        required=False,
    )

    def __init__(self, is_sug, user=None, instance=None, initial=None,
                 data=None, *args, **kwargs):

        if user is not None and instance is not None:
            gt0 = instance.approved_by.filter(user=user).count() > 0
            if initial:
                initial.update({'approve': gt0})
            else:
                initial = {'approve': gt0}

        super(SongForm, self).__init__(
            *args,
            initial=initial,
            data=data,
            instance=instance,
            **kwargs)

        self._is_sug = is_sug

        if is_sug:
            self.fields.pop('available')
            self.fields.pop('approve')

    class Meta:
        model = Song

        fields = [
            'title',
            'artist',
            'length',
            'video_link',
            'approve',
            'available',
        ]

        widgets = {
            'artist': forms.TextInput(),
            'title': forms.TextInput(),
            'length': forms.TimeInput(format='%M:%S'),
        }

    def save(self, user, new=False):
        if new:
            self.instance.author = user
            self.instance.created = datetime.now()
            self.instance.modified = self.instance.created
        else:
            self.instance.modified = datetime.now()

        self.instance.editor = user

        if self._is_sug:
            self.instance.is_suggestion = True
        else:
            self.instance.is_suggestion = False
        try:
            instance = super(SongForm, self).save()
        except IntegrityError:
            self.add_error(
                None, u'Die Song-Artist Kombination ist bereits eingetragen!')

        if 'approve' in self.cleaned_data:
            if self.cleaned_data['approve']:
                instance.approved_by.add(Casler.objects.get(user=user))
            else:
                try:
                    instance.approved_by.remove(Casler.objects.get(user=user))
                except:
                    pass
        return instance


class FilterForm(forms.Form):
    str_filter = forms.CharField(
        widget=forms.TextInput(),
        required=False,
        label=u'Einträge filtern',
    )


class AvailableFilterForm(FilterForm):
    req_available = forms.BooleanField(
        widget=forms.CheckboxInput(),
        required=False,
        label=u'Nur verfügbare',
    )


class SetForm(ModelForm):
    class Meta:
        model = Set

        fields = [
            'title',
            'place',
            'date_played',
            'editable',
        ]

        widgets = {
            'title': forms.TextInput(),
            'place': forms.TextInput(),
            'date_played': forms.DateInput(format='%d.%m.%Y')
        }


class SongOrderWidget(forms.Widget):
    def __init__(self, choice_class=None, movable=False, transferable=None,
                 template=None, *args, **kwargs):
        super(SongOrderWidget, self).__init__(*args, **kwargs)
        self.choice_class = choice_class
        self.movable = movable
        self.transferable = transferable
        self.template = template or 'song/inc_song_order_pos.html'

    def render_song(self, name, pos, song):
        t_song = Song.objects.get(pk=song[0])
        context = {}
        context['class'] = self.choice_class
        context['name'] = name
        context['pos'] = int(pos)
        context['id'] = song[0]
        context['value'] = song[1]
        context['movable'] = self.movable
        context['deprecated'] = not t_song.available
        context['transferable'] = self.transferable
        return render_to_string(self.template, context)

    def render(self, name, value, attrs=None):
        self.attrs.update(attrs)
        starttag = u''.join(
            [u'<ul'] +
            [u' {}="{}"'.format(key, self.attrs[key]) for key in self.attrs] +
            [u'>']
        )

        rv = u''.join(
            [starttag] +
            [self.render_song(name, i, song) for i, song in
             enumerate(self.choices)] +
            [u'</ul>']
        )

        return rv

    def value_from_datadict(self, data, files, name):
        rv = [
            data[sorted_key] for sorted_key in
            sorted([y for y in data if name in y],
                   key=lambda x: int(x.split('_')[-1]))
        ]
        return rv


class SetSongForm(ModelForm):
    songs_available = forms.MultipleChoiceField(
        label=u'Verfügbare Songs',
        required=False,
        widget=SongOrderWidget(
            choice_class=u'setsong_item',
            transferable='right',
            attrs={'class': 'setsong_ul'},
        )
    )
    songs_in_list = forms.MultipleChoiceField(
        label=u'Songs im Set',
        required=False,
        widget=SongOrderWidget(
            choice_class='insetsong_item',
            movable=True,
            transferable='left',
            attrs={'class': 'setsong_ul'},
            template='song/inc_song_order_pos_inlist.html',
        )
    )

    class Meta:
        model = Set
        fields = ['songs_available']

    def __init__(self, request, get=None, instance=None, *args, **kwargs):
        self.request = request
        self.get = get
        super(SetSongForm, self).__init__(instance=instance, *args, **kwargs)
        self.fields['songs_in_list'].choices = instance.set_ordering.\
            values_list('song__id', 'song__title')

        self.fields['songs_available'].choices = Song.availables.\
            exclude(id__in=self.instance.songs.all()).order_by('title').\
            values_list('id', 'title')

    def save(self):
        newsonglist = self.cleaned_data['songs_in_list']
        if self.get is not None:
            dir_ = self.get['dir']
            song_id = self.get['song']

        if dir_ == 'left':
            newsonglist.pop(newsonglist.index(song_id))
        elif dir_ == 'right':
            newsonglist.append(song_id)
        elif dir_ == 'up':
            idx = newsonglist.index(song_id)
            if idx != 0:
                newsonglist[idx], newsonglist[idx - 1] =\
                    newsonglist[idx - 1], newsonglist[idx]
        elif dir_ == 'down':
            idx = newsonglist.index(song_id)
            if idx != len(newsonglist) - 1:
                newsonglist[idx], newsonglist[idx + 1] =\
                    newsonglist[idx + 1], newsonglist[idx]

        self.instance.set_ordering.all().delete()
        order = self.instance.set_ordering
        order.model.objects.bulk_create(
            [order.model(pos=i, set=self.instance, song_id=int(song))
             for i, song in enumerate(newsonglist)])
        self.fields['songs_in_list'].choices = [(
            Song.actual.get(pk=id_).pk,
            Song.actual.get(pk=id_).title) for id_ in newsonglist]
        self.instance.editor = self.request.user
        self.instance.save()
        return self.instance

    def __unicode__(self, *args, **kwargs):
        context = {}
        context['errors'] = self.errors
        context['songs_available'] = self['songs_available']
        context['songs_in_list'] = self['songs_in_list']
        context['set_id'] = self.instance.pk
        context['set_hash'] = self.instance.hash()
        context.update(csrf(self.request))

        return render_to_string('set/inc_set_song_form.html', context)
