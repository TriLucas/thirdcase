from django.core.management.base import BaseCommand
from django.conf import settings

import subprocess


class Command(BaseCommand):
    help = "Dumps the database sql to stdout"

    def handle(self, *args, **options):
        subprocess.call([
            'mysqldump',
            '--host=' + settings.DATABASES['default']['HOST'],
            '--user=' + settings.DATABASES['default']['USER'],
            '--password=' + settings.DATABASES['default']['PASSWORD'],
            '--databases',
            settings.DATABASES['default']['NAME'],
            '--add-drop-database',
            '--skip-comments'])
