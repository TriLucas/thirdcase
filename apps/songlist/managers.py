from django.db.models import Manager, QuerySet


class LengthQueryset(QuerySet):
    def set_length(self):
        return self.extra(select={
            'set_length': 'SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(length)))'
        }).values_list('set_length', flat=True)[0]


class SongManager(Manager):
    def get_queryset(self):
        return LengthQueryset(self.model, using=self._db)


class OrderManager(Manager):
    def get_queryset(self):
        return super(OrderManager, self).get_queryset().order_by('pos')


class SuggestionManager(SongManager):
    def get_queryset(self):
        return super(SuggestionManager, self).get_queryset().filter(
            is_suggestion=True)

    def create(self, is_suggestion=True, *args, **kwargs):
        return super(SuggestionManager, self).create(
            *args,
            is_suggestion=True,
            **kwargs)


class AvailableManager(SongManager):
    def get_queryset(self):
        return super(AvailableManager, self).get_queryset().filter(
            available=True, is_suggestion=False)


class ActualManager(SongManager):
    def get_queryset(self):
        return super(ActualManager, self).get_queryset().filter(
            is_suggestion=False)

    def create(self, is_suggestion=False, *args, **kwargs):
        return super(ActualManager, self).create(
            *args,
            is_suggestion=False,
            **kwargs)
