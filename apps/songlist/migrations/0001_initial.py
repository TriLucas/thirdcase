# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField(verbose_name='Titel')),
                ('artist', models.TextField(default='Third Case', verbose_name='K\xfcnstler')),
                ('length', models.TimeField(default=datetime.time(0, 0), verbose_name='L\xe4nge')),
                ('video_link', models.URLField(default=None, null=True, verbose_name='Beispielvideo', blank=True)),
                ('author', models.ForeignKey(related_name='author', default=1, verbose_name='Autor', blank=True, to=settings.AUTH_USER_MODEL)),
                ('editor', models.ForeignKey(related_name='editor', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='Zuletzt ge\xe4ndert durch')),
            ],
            options={
                'verbose_name': 'Song',
                'verbose_name_plural': 'Songs',
            },
            bases=(models.Model,),
        ),
    ]
