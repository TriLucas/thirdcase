# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='artist',
            field=models.CharField(default='Third Case', max_length=200, verbose_name='K\xfcnstler'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='song',
            name='title',
            field=models.CharField(max_length=200, verbose_name='Titel'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='song',
            unique_together=set([('title', 'artist')]),
        ),
    ]
