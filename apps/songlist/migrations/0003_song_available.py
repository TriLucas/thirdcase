# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0002_auto_20141122_1108'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='available',
            field=models.BooleanField(default=True, verbose_name='Verf\xfcgbar'),
            preserve_default=True,
        ),
    ]
