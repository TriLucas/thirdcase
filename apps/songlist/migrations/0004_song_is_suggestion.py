# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0003_song_available'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='is_suggestion',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
