# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0004_song_is_suggestion'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2013, 1, 1, 8, 0)),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='song',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2013, 1, 1, 8, 0)),
            preserve_default=True,
        ),
    ]
