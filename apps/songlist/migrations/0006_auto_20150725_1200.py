# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('songlist', '0005_auto_20150722_1840'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pos', models.PositiveIntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderManager',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Set',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField(default='', verbose_name='Titel der Setlist')),
                ('place', models.TextField(null=True, verbose_name='Veranstaltungsort', blank=True)),
                ('date_played', models.DateTimeField(default=datetime.datetime(2000, 1, 1, 0, 0), verbose_name='Datum')),
                ('author', models.ForeignKey(related_name='set_owner', default=None, verbose_name='Autor', to=settings.AUTH_USER_MODEL, null=True)),
                ('editor', models.ForeignKey(related_name='set_modifier', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name='Zuletzt ge\xe4ndert durch')),
                ('songs', models.ManyToManyField(related_name='content', through='songlist.Order', to='songlist.Song')),
            ],
            options={
                'verbose_name': 'Setlist',
                'verbose_name_plural': 'Setlisten',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='order',
            name='set',
            field=models.ForeignKey(related_name='set_ordering', to='songlist.Set'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='song',
            field=models.ForeignKey(related_name='song_ordering', to='songlist.Song'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='order',
            unique_together=set([('pos', 'set')]),
        ),
    ]
