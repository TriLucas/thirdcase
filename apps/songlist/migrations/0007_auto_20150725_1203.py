# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def transfer_setlists(apps, schema_editor):
	OldSetList = models.get_model('setlist','Set')
	NewSetList = models.get_model('songlist','Set')

	NewSetOrder = models.get_model('songlist','Order')

	for set_ in OldSetList.objects.all().order_by('pk'):
		newset = NewSetList.objects.create(
			title = set_.title,
			author = set_.author,
			editor = set_.editor,
			place = set_.place,
			date_played = set_.date_played,
		)
		newset.save()

		for order_o in set_.order_set.all().order_by('pos'):
			newset.set_ordering.create(
				pos = order_o.pos,
				song = order_o.song,
			)
		

class Migration(migrations.Migration):

	dependencies = [
		('songlist', '0006_auto_20150725_1200'),
	]

	operations = [
		migrations.RunPython(transfer_setlists),
	]
