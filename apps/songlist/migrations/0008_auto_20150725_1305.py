# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0007_auto_20150725_1203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='set',
            name='songs',
            field=models.ManyToManyField(related_name='in_sets', through='songlist.Order', to='songlist.Song'),
            preserve_default=True,
        ),
    ]
