# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('songlist', '0008_auto_20150725_1305'),
    ]

    operations = [
        migrations.AddField(
            model_name='song',
            name='approved_by',
            field=models.ManyToManyField(default=None, to=settings.AUTH_USER_MODEL, verbose_name=b'Approved by', blank=True),
            preserve_default=True,
        ),
    ]
