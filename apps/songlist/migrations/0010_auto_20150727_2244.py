# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def create_casler(apps, schema_editor):
	User = models.get_model('auth','User')
	Casler = models.get_model('thirdcasler','Casler')

	for user in User.objects.exclude(username='admin'):
		foo = Casler.objects.create(user=user)

class Migration(migrations.Migration):

    dependencies = [
		('thirdcasler', '0001_initial'),
        ('songlist', '0009_song_approved_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='approved_by',
            field=models.ManyToManyField(default=None, to='thirdcasler.Casler', null=True, verbose_name=b'Approved by', blank=True),
            preserve_default=True,
        ),
		migrations.RunPython(create_casler),
    ]
