# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0010_auto_20150727_2244'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='approved_by',
            field=models.ManyToManyField(related_name='approves', default=None, to='thirdcasler.Casler', blank=True, null=True, verbose_name=b'Approved by'),
            preserve_default=True,
        ),
    ]
