# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0011_auto_20150727_2253'),
    ]

    operations = [
        migrations.DeleteModel(
            name='OrderManager',
        ),
    ]
