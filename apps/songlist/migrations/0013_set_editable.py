# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0012_delete_ordermanager'),
    ]

    operations = [
        migrations.AddField(
            model_name='set',
            name='editable',
            field=models.BooleanField(default=True, verbose_name='Kann bearbeitet werden'),
            preserve_default=True,
        ),
    ]
