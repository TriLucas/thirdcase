# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('songlist', '0013_set_editable'),
    ]

    operations = [
        migrations.CreateModel(
            name='PdfErrorLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('log_text', models.TextField(max_length=3000, verbose_name=b'last 3000 logged signs')),
                ('song_sql', models.TextField(max_length=1000, verbose_name=b'SQL for songs:')),
                ('tstamp', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
