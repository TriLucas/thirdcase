# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

from django.utils.functional import lazy
from django.core.urlresolvers import reverse

from managers import SongManager
from managers import SuggestionManager
from managers import ActualManager
from managers import AvailableManager
from managers import OrderManager

from datetime import datetime
from datetime import time

from apps.thirdcasler.models import Casler


class Song(models.Model):
    objects = SongManager()
    availables = AvailableManager()
    suggestions = SuggestionManager()
    actual = ActualManager()

    created = models.DateTimeField(
        default=datetime(2013, 1, 1, 8, 0, 0),
        blank=False,
    )

    modified = models.DateTimeField(
        default=datetime(2013, 1, 1, 8, 0, 0),
        blank=False,
    )

    author = models.ForeignKey(
        User,
        related_name=u'author',
        verbose_name=u'Autor',
        blank=True,
        default=1,
    )
    editor = models.ForeignKey(
        User,
        related_name=u'editor',
        verbose_name=u'Zuletzt geändert durch',
        default=None,
        null=True,
        blank=True,
    )
    title = models.CharField(
        verbose_name=u'Titel',
        max_length=200,
    )
    artist = models.CharField(
        verbose_name=u'Künstler',
        null=False,
        default=u'Third Case',
        max_length=200,
    )
    length = models.TimeField(
        verbose_name=u'Länge',
        null=False,
        default=time(0),
    )
    video_link = models.URLField(
        verbose_name=u'Beispielvideo',
        null=True,
        default=None,
        blank=True,
    )
    available = models.BooleanField(
        verbose_name=u'Verfügbar',
        default=True,
        blank=True
    )
    is_suggestion = models.BooleanField(
        default=False,
        blank=False
    )

    approved_by = models.ManyToManyField(
        Casler,
        blank=True,
        default=None,
        null=True,
        verbose_name="Approved by",
        related_name='approves',
    )

    def _n_occurances(self):
        return u'{}'.format(self.in_sets.count())

    _n_occurances = lazy(_n_occurances, unicode)

    @property
    def n_occurances(self):
        return self._n_occurances()

    class Meta:
        verbose_name = u'Song'
        verbose_name_plural = u'Songs'
        unique_together = ('title', 'artist',)

    def __unicode__(self):
        return u'{} - {}'.format(self.artist, self.title)


class Order(models.Model):
    objects = OrderManager()
    pos = models.PositiveIntegerField()
    set = models.ForeignKey(
        'Set',
        related_name='set_ordering',
    )
    song = models.ForeignKey(
        Song,
        related_name='song_ordering',
    )

    class Meta:
        unique_together = ('pos', 'set',)


class Set(models.Model):
    class Meta:
        verbose_name = u'Setlist'
        verbose_name_plural = u'Setlisten'

    title = models.TextField(
        verbose_name=u'Titel der Setlist',
        null=False,
        default=u'',
    )
    author = models.ForeignKey(
        User,
        related_name=u'set_owner',
        verbose_name=u'Autor',
        default=None,
        null=True,
    )
    editor = models.ForeignKey(
        User,
        related_name=u'set_modifier',
        verbose_name=u'Zuletzt geändert durch',
        default=None,
        null=True,
        blank=True,
    )
    place = models.TextField(
        verbose_name=u'Veranstaltungsort',
        null=True,
        blank=True,
    )
    date_played = models.DateTimeField(
        verbose_name=u'Datum',
        null=False,
        default=datetime(2000, 1, 1),
    )
    songs = models.ManyToManyField(
        Song,
        through=Order,
        related_name='in_sets',
    )

    editable = models.BooleanField(
        verbose_name=u'Kann bearbeitet werden',
        default=True,
        blank=True,
    )

    def get_length(self):
        return self.songs.all().set_length()

    def __unicode__(self):
        return unicode(self.title)

    def get_url(self):
        return reverse('set_detail', kwargs={'set_id': self.pk})

    def hash(self):
        import md5
        return md5.new(
            u''.join([u'{}:{}.'.format(order.song.pk, order.pos) for
                      order in self.set_ordering.all().order_by('pos')])
        ).hexdigest()


class PdfErrorLog(models.Model):
    log_text = models.TextField(
        max_length=3000,
        verbose_name="last 3000 logged signs"
    )

    song_sql = models.TextField(
        max_length=1000,
        verbose_name="SQL for songs:",
    )

    tstamp = models.DateTimeField(
        auto_now=True
    )

    @staticmethod
    def log(log, qset):
        _str, param = qset.query.sql_with_params()
        PdfErrorLog.objects.create(
            log_text=log[-3000:],
            song_sql=_str % param,
        )

    def __unicode__(self):
        return u'{}'.format(self.tstamp)
