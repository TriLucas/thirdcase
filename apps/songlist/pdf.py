# -*- coding: utf-8 -*-
import datetime
import subprocess
import io
import os
import tempfile
import shutil
from apps.songlist.models import PdfErrorLog

from libs.jinja import render_tex_to_string
from django.http import HttpResponse


class PdfError(RuntimeError):
    def __init__(self, msg, log, qset):
        super(PdfError, self).__init__(msg)
        self.log = log

        PdfErrorLog.log(log, qset)

    def get_log(self):
        return self.log


def build_pdf(title, song_qset, date=None, length=None):
    tmp_folder = tempfile.mkdtemp(suffix="thirdcase_pdf")

    try:
        tmp_tex_file = os.path.join(tmp_folder, 'tmp.tex')
        tmp_pdf_file = os.path.join(tmp_folder, 'tmp.pdf')
        tmp = unicode(render_tex_to_string("tex/page.tex", {
            'title': title,
            'date': date,
            'songs': song_qset.iterator(),
            'length': length,
        }))
        tmp_file = io.open(tmp_tex_file, 'w', encoding='utf8')
        tmp_file.write(tmp)
        tmp_file.close()
        _null = open(os.devnull, 'w')
        cmd = [
            'pdflatex',
            '-output-directory',
            tmp_folder,
            '-interaction',
            'nonstopmode',
            tmp_tex_file
        ]
        for x in range(0, 3):
            code = subprocess.call(cmd, stdout=_null, stderr=subprocess.STDOUT)
            if code != 0:
                error_data = io.open(os.path.join(tmp_folder, 'tmp.log'), 'r')
                log = error_data.read()
                error_data.close()
                raise PdfError(msg='Fehler beim erstellen der PDF', log=log,
                               qset=song_qset)

        with io.open(tmp_pdf_file, 'rb') as pdf:
            response = HttpResponse(pdf.read(), content_type='application/pdf')
            key = 'Content-Disposition'
            response[key] = u'inline;filename={}{}.pdf'.format(
                title.replace(' ', '_'),
                datetime.date.today().strftime('%d_%m_%Y'))
            pdf.close()
        return response
    finally:
        shutil.rmtree(tmp_folder)
