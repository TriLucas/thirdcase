# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from apps.songlist import views

urlpatterns = patterns(
    '',
    url(r'^overview/$', views.overview, name='song_overview'),
    url(r'^(?P<song_id>\d+)/$', views.edit, name='song_detail'),
    url(r'^add/$', views.add, name='song_add'),
    url(r'^suggestion/overview/$', views.sug_overview,
        name='sug_song_overview'),
    url(r'^suggestion/(?P<song_id>\d+)/$', views.sug_edit,
        name='sug_song_detail'),
    url(r'^suggestion/add/$', views.sug_add, name='sug_song_add'),
    url(r'^suggestion/delete/(?P<song_id>\d+)/$', views.sug_delete,
        name='sug_song_delete'),
    url(r'^suggestion/transfer/(?P<song_id>\d+)/$', views.sug_transfer,
        name='sug_song_transfer'),
    url(r'^setlist/overview/$', views.set_overview, name='set_overview'),
    url(r'^setlist/add/$', views.set_add, name='set_add'),
    url(r'^setlist/detail/(?P<set_id>\d+)/$', views.set_detail,
        name='set_detail'),
    url(r'^setlist/songs/ajax/$', views.ajax, name='ajax_songs'),
    url(r'^setlist/songs/(?P<set_id>\d+)/$', views.set_songs,
        name='set_edit'),
    url(r'^setlist/del/(?P<set_id>\d+)/$', views.set_delete,
        name='set_del'),
    url(r'^setlist/export/(?P<set_id>\d+)/$', views.set_export,
        name='set_export'),
    url(r'^setlist/export/available/$', views.set_export_available,
        name='set_export_available'),
    url(r'^setlist/edit/(?P<set_id>\d+)/$', views.set_edit,
        name='set_edit_details'),
    url(r'^setlist/hash/(?P<set_id>\d+)/$', views.gethash, name='ajax_hash'),
)
