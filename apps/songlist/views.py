# -*- coding: utf-8 -*-
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.views.decorators.cache import never_cache
from django.shortcuts import redirect
from django.db.models import Q
from django.http import HttpResponse

from libs.jinja import render_to_response

from apps.songlist.models import Song
from apps.songlist.models import Set
from apps.songlist.forms import SongForm
from apps.songlist.forms import FilterForm
from apps.songlist.forms import AvailableFilterForm
from apps.songlist.forms import SetForm
from apps.songlist.forms import SetSongForm

from apps.songlist.pdf import build_pdf


@login_required(login_url=reverse_lazy('login'))
def overview(request, is_sug=False):
    songs = None
    context = {}

    if request.method == 'POST':
        post_data = request.POST
    else:
        post_data = None

    filter_form = (FilterForm if is_sug
                   else AvailableFilterForm)(data=post_data)
    if filter_form.is_valid():
        artitle = filter_form.cleaned_data.get('str_filter')
        if is_sug:
            songs = Song.suggestions
        else:
            if filter_form.cleaned_data.get('req_available'):
                songs = Song.availables
            else:
                songs = Song.actual
        items = songs.filter(
            Q(title__icontains=artitle) | Q(artist__icontains=artitle)
        ).order_by('artist', 'title')
    else:
        artitle = ''
        if is_sug:
            songs = Song.suggestions
        else:
            songs = Song.actual
        items = songs.all().order_by('artist', 'title')

    alphabetic_tree = {}
    for item in items:
        if unicode(item).upper().find(artitle.upper()) != -1:
            if item.artist[0].upper() not in alphabetic_tree:
                alphabetic_tree[item.artist[0].upper()] = []
            alphabetic_tree[item.artist[0].upper()].append(item)

    context.update(csrf(request))
    context['active'] = 'song' if not is_sug else 'sug_song'
    context['sub_active'] = 'overview'
    context['tree'] = alphabetic_tree
    context['filter_form'] = filter_form
    if not is_sug:
        template = 'song/inc_songs.html'
    else:
        template = 'song/inc_songs_sug.html'
    return render_to_response(template, context)


@login_required(login_url=reverse_lazy('login'))
def edit(request, song_id, is_sug=False):
    context = {}
    if request.method == 'POST':
        post_data = request.POST
    else:
        post_data = None

    song = Song.objects.get(pk=song_id)
    the_form = SongForm(data=post_data, is_sug=is_sug, instance=song,
                        user=request.user)

    if request.method == 'POST' and the_form.is_valid():
        if the_form.has_changed():
            the_form.save(request.user)
        return redirect(overview if not is_sug else sug_overview)

    context.update(csrf(request))
    context['active'] = 'song' if not is_sug else 'sug_song'
    context['sub_active'] = 'insert'
    context['form'] = the_form
    context['song'] = song
    return render_to_response('song/inc_songs_form.html', context)


@login_required(login_url=reverse_lazy('login'))
def add(request, is_sug=False):
    context = {}
    if request.method == 'POST':
        post_data = request.POST
    else:
        post_data = None

    the_form = SongForm(data=post_data, is_sug=is_sug)

    if request.method == 'POST' and the_form.is_valid():
        the_form.save(request.user, True)
        context['new_song'] = '{} - {}'.format(the_form.cleaned_data['artist'],
                                               the_form.cleaned_data['title'])
        if the_form.is_valid():
            the_form = SongForm(is_sug=is_sug)

    context.update(csrf(request))
    context['active'] = 'song' if not is_sug else 'sug_song'
    context['sub_active'] = 'insert'
    context['form'] = the_form
    return render_to_response('song/inc_songs_form.html', context)


@login_required(login_url=reverse_lazy('login'))
def sug_overview(request):
    return overview(request, True)


@login_required(login_url=reverse_lazy('login'))
def sug_edit(request, song_id):
    return edit(request, song_id, True)


@login_required(login_url=reverse_lazy('login'))
def sug_add(request):
    return add(request, True)


@login_required(login_url=reverse_lazy('login'))
def sug_delete(request, song_id):
    context = {}

    the_song = Song.objects.get(pk=song_id)
    if request.method == 'POST':
        the_song.delete()
        return redirect(sug_overview)

    context.update(csrf(request))
    context['song'] = the_song
    context['active'] = 'sug_song'
    return render_to_response('song/inc_song_delete.html', context)


@login_required(login_url=reverse_lazy('login'))
def sug_transfer(request, song_id):
    context = {}

    the_song = Song.suggestions.get(pk=song_id)
    if request.method == 'POST':
        the_song.is_suggestion = False
        the_song.save()
        return redirect(overview)

    context.update(csrf(request))
    context['active'] = 'sug_cong'
    return render_to_response('song/inc_song_transfer.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_delete(request, set_id):
    context = {}

    the_set = Set.objects.get(pk=set_id)

    if request.method == 'POST':
        the_set.delete()
        return redirect(set_overview)

    context.update(csrf(request))
    context['set'] = the_set
    context['active'] = 'set'
    return render_to_response('set/inc_set_delete.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_edit(request, set_id):
    context = {}
    posting = False

    if request.method == 'POST':
        posting = True
        post_data = request.POST
    else:
        post_data = None

    the_set = Set.objects.get(pk=set_id)
    the_form = SetForm(data=post_data, instance=the_set)

    if posting and the_form.is_valid():
        the_form.save()
        return redirect(reverse_lazy(
            'set_detail',
            kwargs={'set_id': set_id}
        ))

    context['form'] = the_form
    context['active'] = 'set'
    context.update(csrf(request))
    return render_to_response('set/inc_set_edit_details.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_songs(request, set_id):
    context = {}
    posting = False

    if request.method == 'POST':
        post_data = request.POST
        posting = True
    else:
        post_data = None

    the_set = Set.objects.get(pk=set_id)
    if not the_set.editable:
        return redirect(reverse_lazy('set_detail', kwargs={'set_id': set_id}))
    the_form = SetSongForm(request=request, data=post_data, instance=the_set)

    if posting and the_form.is_valid():
        the_form.save()
        return redirect(reverse_lazy(
            'set_detail',
            kwargs={'set_id': set_id}
        ))

    context.update(csrf(request))
    context['form'] = the_form
    context['set'] = the_set
    context['active'] = 'set'
    return render_to_response('set/inc_set_edit.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_detail(request, set_id):
    context = {}

    the_set = Set.objects.get(pk=set_id)
    length = the_set.get_length()

    context.update(csrf(request))
    context['set'] = the_set
    context['songs'] = the_set.set_ordering.iterator()
    context['active'] = 'set'
    context['length'] = length
    return render_to_response('set/inc_set_detail.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_overview(request):
    context = {}

    sets = Set.objects.all().order_by('-date_played', 'title')
    context.update(csrf(request))
    context['active'] = 'set'
    context['sub_active'] = 'overview'
    context['sets'] = sets
    return render_to_response('set/inc_set_overview.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_add(request):
    context = {}

    post_data = request.POST or None

    the_form = SetForm(data=post_data)
    if the_form.is_valid():
        the_form.save()
        return redirect(set_overview)

    context.update(csrf(request))
    context['form'] = the_form
    context['active'] = 'set'
    context['sub_active'] = 'insert'
    return render_to_response('set/inc_set_add.html', context)


@login_required(login_url=reverse_lazy('login'))
def set_export(request, set_id=None, available=False):
    if available:
        qset = Song.availables.all()
        return build_pdf(
            title=u'Übersicht',
            song_qset=qset.order_by('title', 'artist'),
            length=qset.set_length(),
        )
    the_set = Set.objects.get(pk=set_id)
    return build_pdf(
        title=the_set.title,
        date=the_set.date_played.strftime('%d.%m.%Y'),
        length=the_set.get_length(),
        song_qset=the_set.songs.all().order_by('song_ordering__pos')
    )


@login_required(login_url=reverse_lazy('login'))
def set_export_available(request):
    return set_export(request, available=True)


@never_cache
@login_required(login_url=reverse_lazy('login'))
def ajax(request):
    the_set = Set.objects.get(pk=request.POST['set_id'])
    assert the_set.editable
    the_form = SetSongForm(request=request, get=request.GET, data=request.POST,
                           instance=the_set)
    if the_form.is_valid():
        newinstance = the_form.save()
        the_form = SetSongForm(instance=newinstance, request=request)
    return HttpResponse(unicode(the_form))


@never_cache
@login_required(login_url=reverse_lazy('login'))
def gethash(request, set_id):
    import json
    rv = {}

    the_set = Set.objects.get(pk=set_id)

    rv['editor'] = unicode(the_set.editor)
    rv['hash'] = the_set.hash()
    return HttpResponse(json.dumps(rv))
