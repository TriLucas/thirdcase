# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User


class PwForm(forms.Form):
    old_pw = forms.CharField(
        widget=forms.PasswordInput(),
        label=u'Aktuelles Passwort',
    )
    new_pw = forms.CharField(
        widget=forms.PasswordInput(),
        label=u'Neues Passwort'
    )
    new_pw_repeat = forms.CharField(
        widget=forms.PasswordInput(),
        label=u'Neues Passwort(wiederholt)'
    )

    def is_valid(self, user):
        state = super(PwForm, self).is_valid()
        old_pw = self.cleaned_data.get('old_pw')
        new_pw = self.cleaned_data.get('new_pw')
        new_pw_repeat = self.cleaned_data.get('new_pw_repeat')
        if old_pw:
            myuser = User.objects.get(username=user)
            auth_user = myuser.check_password(old_pw)
            if not auth_user:
                self.add_error(None, u'Falsches Passwort!')

        pw_are_equal = False
        if new_pw and new_pw_repeat:
            pw_are_equal = new_pw == new_pw_repeat
            if not pw_are_equal:
                self.add_error(None, u'Passwörter stimmen nicht überein!')

        return state and auth_user and pw_are_equal
