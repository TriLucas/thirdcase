import datetime
import requests
import subprocess

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings


class Command(BaseCommand):
    def get_ipv6(self):
        out = subprocess.check_output(['ip', '-6',  'addr',  'list',
                                          'scope', 'global'])
        return out[out.find('inet6') + 6:out.find('/64')]
    def handle(self, *args, **options):
        v6_url = ('http://dynv6.com/api/update?hostname={}&'
                  'ipv6={}&token={}').format(
                    settings.DYNV6NET_HOSTNAME,
                    self.get_ipv6(),
                    settings.DYNV6NET_TOKEN)
        v4_url = ('http://ipv4.dynv6.com/api/update?hostname={}&ipv4=auto&'
                  'token={}').format(
                    settings.DYNV6NET_HOSTNAME,
                    settings.DYNV6NET_TOKEN)
        for url in {v4_url, v6_url}:
            result = requests.get(url)
            if result.status_code != 200:
                raise CommandError(
                    'Unable to update ddns, reason: ' + result.text)
            return "Set to {} @ {} ".format(
                self.get_ipv6(),
                str(datetime.datetime.now()),
            )
