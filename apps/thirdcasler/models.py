# -*- coding: utf-8 -*coding: utf-8 -*--
from django.db import models
from django.contrib.auth.models import User


class Casler(models.Model):
    approval_image = models.CharField(
        verbose_name=u'Approval Bild',
        blank=False,
        default='puppe.png',
        max_length=400,
    )

    user = models.OneToOneField(User, blank=False, default=1)

    def __unicode__(self):
        return unicode(self.user)
