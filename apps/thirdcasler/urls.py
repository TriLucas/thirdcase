# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.thirdcasler import views

urlpatterns = patterns(
    '',
    url(r'$', views.changepw, name='profile')
)
