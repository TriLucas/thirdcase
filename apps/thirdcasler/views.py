# -*- coding utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User
from apps.thirdcasler.forms import PwForm

from libs.jinja import render_to_response


@login_required(login_url=reverse_lazy('login'))
def changepw(request):
    context = {}

    if request.method == 'POST':
        post_data = request.POST
    else:
        post_data = None

    the_form = PwForm(post_data)
    if request.method == 'POST' and the_form.is_valid(request.user):
        myuser = User.objects.get(username=request.user)
        myuser.set_password(the_form.cleaned_data.get('new_pw'))
        myuser.save()
        context['success'] = True
    else:
        context['success'] = False
    context['form'] = the_form
    context['active'] = 'profile'
    context.update(csrf(request))

    return render_to_response('main/inc_pw.html', context)
