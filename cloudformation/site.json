{
	"AWSTemplateFormatVersion" : "2010-09-09",
	"Description" : "Site stack",
	"Parameters" : {
		"KeyName" : {
			"Description" : "Name of an existing EC2 KeyPair to enable SSH access to the instances",
			"Default" : "tristan",
			"Type" : "String",
			"MinLength" : "1",
			"MaxLength" : "255",
			"AllowedPattern" : "[\\x20-\\x7E]*",
			"ConstraintDescription" : "can contain only ASCII characters."
		},
		"GitHead" : {
			"Description" : "GitHead to use",
			"Default" : "master",
			"Type" : "String"
		},
		"AppServerInstanceType" : {
			"Description" : "EC2-Instance Type",
			"Type" : "String",
			"AllowedValues" : ["t2.nano", "t2.micro"],
			"Default" : "t2.nano"
		},
		"AdminIp" : {
			"Description" : "Public IP of managing machine",
			"Type" : "String",
			"Default" : "127.0.0.1"
		},
		"TestStack" : {
			"Description" : "Launched Stack is a test-Stack",
			"Type" : "String",
			"AllowedValues" : ["True", "False"],
			"Default" : "True"
		},
		"LetsencryptVersion": {
			"Description": "Version of letsencrypt to use",
			"Type": "String",
			"Default": "v0.8.0"
		},
		"OverwriteDatabase" : {
			"Description" : "Overwrite Database with dump",
			"Type" : "String",
			"AllowedValues" : ["True", "False"],
			"Default" : "False"
		},
		"DumpFile" : {
			"Description" : "SQL-Dump filename",
			"Type" : "String"
		},
		"DatabasePassword" : {
			"Description" : "Passwort for the database",
			"Type" : "String",
			"NoEcho" : "True"
		}
	},
	"Conditions" : {
		"IsTest" : {
			"Fn::Equals" :  [
				"True",
				{"Ref" : "TestStack"}
			]
		},
		"IsNotTest" : {
			"Fn::Equals" : [
				"False",
				{"Ref" : "TestStack"}
			]
		},
		"OverwriteDB" : {
			"Fn::Equals" :  [
				"True",
				{"Ref" : "OverwriteDatabase"}
			]
		}
	},
	"Resources" : {
		"AppServerRole" : {
			"Type" : "AWS::IAM::Role",
			"Properties" : {
				"AssumeRolePolicyDocument" : {
					"Statement" : [{
						"Effect" : "Allow",
						"Principal" : {
							"Service" : ["ec2.amazonaws.com"]
						},
						"Action" : "sts:AssumeRole"
					}]
				},
				"Path" : "/"
			}
		},
		"AppServerRolePolicies" : {
			"Type" : "AWS::IAM::Policy",
			"Properties" : {
				"PolicyName" : "S3Download",
				"PolicyDocument" : {
					"Statement" : [{
						"Action" : [
							"s3:GetObject",
							"s3:ListObjects",
							"s3:ListBucket",
							"s3:PutObject"
						],
						"Effect" : "Allow",
						"Resource" : [
							"arn:aws:s3:::tc-keys/id_rsa",
							"arn:aws:s3:::tc-dumps/*",
							"arn:aws:s3:::thirdcase-certs/*",
							"arn:aws:s3:::thirdcase-certs"
						]
					}]
				},
				"Roles" : [{"Ref" : "AppServerRole"}]
			}
		},
		"AppServerProfile" : {
			"Type" : "AWS::IAM::InstanceProfile",
			"Properties" : {
				"Roles" : [{"Ref" : "AppServerRole"}],
				"Path" : "/"
			}
		},
		"AppServer" : {
			"Type" : "AWS::EC2::Instance",
			"DependsOn" : "Database",
			"Properties" : {
				"IamInstanceProfile" : {"Ref" : "AppServerProfile"},
				"AvailabilityZone" : "eu-west-1a",
				"DisableApiTermination" : false,
				"EbsOptimized" : false,
				"ImageId" : "ami-bff32ccc",
				"InstanceInitiatedShutdownBehavior" : "terminate",
				"InstanceType" : {"Ref" : "AppServerInstanceType"},
				"KeyName" : { "Ref" : "KeyName" },
				"Monitoring" : false,
				"SecurityGroupIds" : [{"Ref" : "AppServerSecurityGroup"}],
				"UserData" : {
					"Fn::Base64" : {
						"Fn::Join" : ["", [
							"#!/bin/bash\n",
							"sed -i \"s/ZONE=\\\"UTC\\\"/ZONE=\\\"Europe\\/Berlin\\\"/g\" /etc/sysconfig/clock\n",
							"ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime\n",
							"yum check-update\n",
							"yum update -y\n",
							"/opt/aws/bin/cfn-init ",
							" --stack ", { "Ref" : "AWS::StackName" },
							" --resource AppServer",
							" --region ", { "Ref" : "AWS::Region" }, "\n"
						]]
					}
				}
			},
			"Metadata" : {
				"AWS::CloudFormation::Authentication" : {
					"S3AccessCreds" : {
						"type" : "S3",
						"roleName" : {"Ref" : "AppServerRole"}
					}
				},
				"AWS::CloudFormation::Init" : {
					"config" :{
						"packages" : {
							"yum" : {
								"vim-enhanced" : [],
								"htop" : [],
								"sysstat" : [],
								"httpd" : [],

								"ftp" : [],
								"python27-devel" : [],
								"mod_wsgi-python27" : [],
								"mod_ssl" : [],

								"git" : [],
								"gcc" : [],

								"mysql-devel" : [],

								"texlive*": []
							},
							"python": {
								"MySQL-python" : [],

								"django" : ["1.7.1"],
								"jinja2" : [],
								"ipython" : ["1.2.1"]
							}
						},
						"commands" : {
							"01-create-swap" : {
								"command" : {"Fn::Join": ["", [
									"if [ ! -f /swapfile ] ; then ",
									"dd if=/dev/zero of=/swapfile bs=1024 count=524288 && ",
									"chmod 600 /swapfile && ",
									"mkswap /swapfile && ",
									"swapon /swapfile ; fi"
								]]}
							},
							"02-download-public-key" : {
								"command" : {"Fn::Join" : ["", [
									"su ec2-user -c \"",
									"if [ ! -f id_rsa ] ; then aws s3api get-object --bucket tc-keys --key id_rsa id_rsa && ",
									"chmod 400 id_rsa ; fi",
									"\""
								]]},
								"cwd" : "/home/ec2-user/.ssh"
							},
							"03-add-gitlab-hostkey" : {
								"command" : {"Fn::Join" : ["", [
									"if ! grep -q gitlab /home/ec2-user/.ssh/known_hosts ; ",
									"then su ec2-user -c \"ssh-keyscan -t rsa gitlab.com >> /home/ec2-user/.ssh/known_hosts\" ; ",
									"fi"
								]]}
							},
							"04-git-clone-letsencrypt" : {
								"command": {"Fn::Join": ["", [
									"if [ ! -d /opt/letsencrypt/.git ] ; ",
									"then git clone https://github.com/letsencrypt/letsencrypt ; ",
									"fi"
								]]},
								"cwd": "/opt"
							},
							"05-git-reset-letsencrypt" : {
								"command": {"Fn::Join": ["", [
									"git reset --hard ",
									{"Ref": "LetsencryptVersion"}
								]]},
								"cwd": "/opt/letsencrypt"
							},
							"06-git-clone-thirdcase" : {
								"command" : {"Fn::Join" : ["", [
									"if [ ! -d /var/www/thirdcase/.git ] ; then chown -R ec2-user /var/www && ",
									"su ec2-user -c \"",
									"git clone --no-checkout --recursive git@gitlab.com:TriLucas/thirdcase.git /tmp/thirdcase && ",
									"mkdir -p /var/www/thirdcase && ",
									"mv /tmp/thirdcase/.git/ /var/www/thirdcase/ && ",
									"rm -r /tmp/thirdcase/ ",
									"\" ; fi"
								]]},
								"cwd" : "/var/www"
							},
							"07-git-fetch" : {
								"command" : {"Fn::Join" : ["", [
									"su ec2-user -c \"",
									"git fetch origin ",
									"\""
								]]},
								"cwd" : "/var/www/thirdcase"
							},
							"08-git-reset" : {
								"command" : {"Fn::Join" : ["", [
									"su ec2-user -c \"",
									"git reset --hard ", {"Ref" : "GitHead"},
									"\""
								]]},
								"cwd" : "/var/www/thirdcase"
							},
							"09-git-submodule-init" : {
								"command" : "su ec2-user -c \"git submodule init \"",
								"cwd" : "/var/www/thirdcase"
							},
							"10-git-submodule-foreach": {
								"command": "su ec2-user -c \"git submodule foreach 'git fetch --tags'\"",
								"cwd" : "/var/www/thirdcase"
							},
							"11-git-submodule-update": {
								"command": "su ec2-user -c \"git submodule update\"",
								"cwd" : "/var/www/thirdcase"
							},
							"12-pipe-sqldump" : {
								"command" :
								{"Fn::If" : [
									"OverwriteDB",
									{"Fn::Join" : ["", [
										" mkdir -p /tmp/dump && chown ec2-user /tmp/dump && ",
										"su ec2-user -c \"aws s3api get-object --bucket tc-dumps --key ",
										{"Ref" : "DumpFile"}, " /tmp/dump/", {"Ref" : "DumpFile"}, "\" && ",
										"bzcat /tmp/dump/", {"Ref" : "DumpFile"}, " | /var/www/thirdcase/manage.py dbshell && ",
										"rm -rf /tmp/dump"
									]]},
									"echo \"Nothing to do\""
								]}
							},
							"13-download-certs" : {
								"command": {"Fn::Join": ["", [
									"mkdir -p /etc/letsencrypt/live/", {"Ref" : "AWS::StackName"},
									".thirdcase.de && aws s3 sync s3://thirdcase-certs/ ",
									"/etc/letsencrypt/live/", {"Ref" : "AWS::StackName"},
									".thirdcase.de"
								]]}
							},
							"14-reload-cron" : {
								"command" : "/etc/init.d/crond reload"
							},
							"15-cold-start-httpd" : {
								"command" : "mv /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.conf.bak ; /etc/init.d/httpd restart"
							}
						},
						"files" : {
							"/etc/httpd/conf.d/thirdcase.conf" : {
								"content" : {"Fn::Join" : ["", [
"\nLoadModule wsgi_module modules/mod_wsgi.so",
"\nLoadModule ssl_module modules/mod_ssl.so",
"\nLoadModule alias_module modules/mod_alias.so",
"\nLoadModule rewrite_module modules/mod_rewrite.so",
"\nLoadModule log_config_module modules/mod_log_config.so",
"\n",
"\nAlias /static/admin/ /usr/local/lib/python2.7/site-packages/Django-1.7.1-py2.7.egg/django/contrib/admin/static/admin/",
"\nAlias /static/ /var/www/thirdcase/static/",
"\nAlias /.well-known/ /tmp/letsencrypt/.well-known/",
"\n",
"\n ServerTokens ProductOnly",
"\n",
"\n<VirtualHost *:80>",
{"Fn::If" : [
		"IsTest",
		{"Fn::Join" : ["", ["\n        ServerName ", {"Ref" : "AWS::StackName"}, ".thirdcase.de"]]},
		{"Fn::Join" : ["", [
			"\n        ServerName ", {"Ref" : "AWS::StackName"}, ".thirdcase.de",
			"\n        ServerAlias thirdcase.de"
		]]}
	]
},
"\n        ServerAdmin tristan@thirdcase.de",
"\n",
"\n        DocumentRoot /var/www/thirdcase",
"\n",
"\n        WSGIScriptAlias / /var/www/thirdcase/thirdcase/wsgi.py",
"\n",
"\n        <Directory /var/www/thirdcase>",
"\n                <Files wsgi.py>",
"\n                </Files>",
"\n        </Directory>",
"\n",
"\n        <Directory /usr/local/lib/python2.7/site-packages/Django-1.7.1-py2.7.egg/django/contrib/admin/static/admin>",
"\n        </Directory>",
"\n",
"\n        <Directory /tmp/letsencrypt/.well-known>",
"\n        </Directory>",
"\n    RewriteEngine on",
"\n    RewriteCond %{REQUEST_URI} !^/.well-known*",
"\n    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [L,QSA,R=permanent]",
"\n</VirtualHost>",
"\n",
"\nListen 443",
"\n",
"\n<VirtualHost *:443>",
{"Fn::If" : [
		"IsTest",
		{"Fn::Join" : ["", ["\n        ServerName ", {"Ref" : "AWS::StackName"}, ".thirdcase.de"]]},
		{"Fn::Join" : ["", [
			"\n        ServerName ", {"Ref" : "AWS::StackName"}, ".thirdcase.de",
			"\n        ServerAlias thirdcase.de"
		]]}
	]
},
"\n        ServerAdmin tristan@thirdcase.de",
"\n",
"\n        DocumentRoot /var/www/thirdcase",
"\n",
"\n        WSGIScriptAlias / /var/www/thirdcase/thirdcase/wsgi.py",
"\n",
"\n        <Directory /var/www/thirdcase>",
"\n                <Files wsgi.py>",
"\n                </Files>",
"\n        </Directory>",
"\n",
"\n        <Directory /usr/local/lib/python2.7/site-packages/Django-1.7.1-py2.7.egg/django/contrib/admin/static/admin>",
"\n        </Directory>",
"\n",
"\n    SSLCertificateFile /etc/letsencrypt/live/", {"Ref" : "AWS::StackName"}, ".thirdcase.de/cert.pem",
"\n    SSLCertificateChainFile /etc/letsencrypt/live/", {"Ref" : "AWS::StackName"}, ".thirdcase.de/fullchain.pem",
"\n    SSLCertificateKeyFile /etc/letsencrypt/live/", {"Ref": "AWS::StackName"}, ".thirdcase.de/privkey.pem",
"\n",
"\n\t# Baseline setting to Include for SSL sites",
"\n",
"\n\tSSLEngine on",
"\n\t",
"\n\t# Intermediate configuration, tweak to your needs",
"\n\tSSLProtocol all -SSLv2 -SSLv3",
"\n\tSSLCipherSuite ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA",
"\n\tSSLHonorCipherOrder on",
"\n\tSSLCompression off",
"\n",
"\n\tSSLOptions +StrictRequire",
"\n\t",
"\n\t# Add vhost name to log entries:",
"\n\tLogFormat \"%h %l %u %t \\\"%r\\\" %>s %b \\\"%{Referer}i\\\" \\\"%{User-agent}i\\\"\" vhost_combined",
"\n\tLogFormat \"%v %h %l %u %t \\\"%r\\\" %>s %b\" vhost_common",
"\n",
"\n\t#CustomLog /var/log/apache2/access.log vhost_combined",
"\n\t#LogLevel warn",
"\n\t#ErrorLog /var/log/apache2/error.log",
"\n\t",
"\n\t# Always ensure Cookies have \"Secure\" set (JAH 2012/1)",
"\n\t#Header edit Set-Cookie (?i)^(.*)(;\\s*secure)??((\\s*;)?(.*)) \"$1; Secure$3$4\"",
"\n</VirtualHost>",
"\n"
								]]}
							},
							"/var/www/thirdcase/thirdcase/local_settings.py" : {
								"content" : {"Fn::Join" : ["", [
"\nDEBUG = ", {"Fn::If" : ["IsTest", "True", "False"]},
"\nTEMPLATE_DEBUG = False",
"\n",
"\nALLOWED_HOSTS = (",
"\n\t'www.thirdcase.de',",
"\n\t'thirdcase.de',",
"\n)",
"\n",
"\nDATABASES = {",
"\n\t'default': {",
"\n\t\t'ENGINE': 'django.db.backends.mysql',",
"\n\t\t'NAME': 'db_thirdcase',",
"\n\t\t'USER': 'thirdcase',",
"\n\t\t'PASSWORD':'", {"Ref" : "DatabasePassword"}, "',",
"\n\t\t'HOST': '", {"Fn::GetAtt" : ["Database" , "Endpoint.Address"]} ,"',",
"\n\t}",
"\n}",
"\n"
								]]},
								"mode": "000644",
								"owner": "ec2-user",
								"group": "ec2-user"
							},
							"/var/spool/cron/ec2-user": {
								"mode": "000644",
								"owner": "ec2-user",
								"group": "ec2-user",
								"content": {"Fn::Join": ["", [
									"10 2 * * * /var/www/thirdcase/scripts/backup.sh\n",
									"0 0 * * * /var/www/thirdcase/scripts/renew_cert.sh ",
									{ "Ref" : "AWS::StackName" }, "\n"
								]]}
							},
							"/etc/cfn/cfn-hup.conf" : {
								"content" : { "Fn::Join" : ["", [
									"[main]\n",
									"stack=", { "Ref" : "AWS::StackId" }, "\n",
									"region=", { "Ref" : "AWS::Region" }, "\n",
									"verbose=true\n",
									"interval=1\n"
								]]},
								"mode": "000644",
								"owner": "root",
								"group": "root"
							},
							"/etc/cfn/hooks.d/cfn-auto-reloader.conf" : {
								"content": { "Fn::Join" : ["", [
									"[cfn-auto-reloader-hook]\n",
									"triggers=post.update\n",
									"path=Resources.AppServer.Metadata.AWS::CloudFormation::Init\n",
									"action=/opt/aws/bin/cfn-init",
									" --stack ", { "Ref" : "AWS::StackName" },
									" --resource AppServer ",
									" --region ", { "Ref" : "AWS::Region" }, "\n",
									"runas=root\n"
								]]}
							}
						},
						"services" : {
							"sysvinit": {
								"cfn-hup": {
									"enabled": "true",
									"ensureRunning": "true",
									"files": [
										"/etc/cfn/cfn-hup.conf",
										"/etc/cfn/hooks.d/cfn-auto-reloader.conf"
								]}
							}
						},
						"sources" : {
						}
					}
				}
			}
		},
		"Database" : {
			"Type" : "AWS::RDS::DBInstance",
			"Properties" : {
				"AllocatedStorage" : "100",
				"AllowMajorVersionUpgrade" : true,
				"AvailabilityZone" : "eu-west-1a",
				"DBInstanceClass" : "db.t2.micro",
				"DBName" : "db_thirdcase",
				"DBSecurityGroups" : [{"Ref" : "DBSecurityGroup"}],
				"Engine" : "MySQL",
				"MasterUsername" : "thirdcase",
				"MasterUserPassword" : { "Ref" : "DatabasePassword" },
				"BackupRetentionPeriod" : 0,
				"MultiAZ" : false,
				"PubliclyAccessible" : false
			}
		},
		"AppServerSecurityGroup" : {
			"Type" : "AWS::EC2::SecurityGroup",
			"Properties" : {
				"GroupDescription" : "TCP-Rule for AppServer",
				"SecurityGroupIngress" : [
					{
						"IpProtocol" : "TCP",
						"FromPort" : "80",
						"ToPort" : "80",
						"CidrIp" : "0.0.0.0/0"
					}, {
						"IpProtocol" : "TCP",
						"FromPort" : "443",
						"ToPort" : "443",
						"CidrIp" : "0.0.0.0/0"
					}, {
						"IpProtocol" : "TCP",
						"FromPort" : "20",
						"ToPort" : "22",
						"CidrIp" : {"Ref" : "AdminIp"}
					}
				]
			}
		},
		"DBSecurityGroup" : {
			"Type" : "AWS::RDS::DBSecurityGroup",
			"Properties" : {
				"DBSecurityGroupIngress" : [{
					"CIDRIP" : "0.0.0.0/0"
				}],
				"GroupDescription" : "Allowing Appserver -> RDS"
			}
		},
		"PDns" : {
			"Type" : "AWS::Route53::RecordSet",
			"Condition" : "IsTest",
			"Properties" : {
				"HostedZoneId" : "Z1LFR6TUTM940N",
				"Name" : {"Fn::Join" : ["", [ {"Ref" : "AWS::StackName"} , ".thirdcase.de."]]},
				"Type" : "A",
				"TTL" : "900",
				"ResourceRecords" : [ {"Fn::GetAtt" : [ "AppServer", "PublicIp"]} ]
			}
		},
		"PRootDns" : {
			"Type" : "AWS::Route53::RecordSet",
			"Condition" : "IsNotTest",
			"Properties" : {
				"HostedZoneId" : "Z1LFR6TUTM940N",
				"Name" : "thirdcase.de",
				"Type" : "A",
				"TTL" : "900",
				"ResourceRecords" : [ {"Fn::GetAtt" : [ "AppServer", "PublicIp"]} ]
			}
		}
	}
}
