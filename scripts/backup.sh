#!/bin/bash

DATE=`date +%Y%m%d`

/var/www/thirdcase/manage.py mysqldump | bzip2 > /tmp/dump_$DATE.bz2
aws s3 cp /tmp/dump_$DATE.bz2 s3://tc-dumps/
rm /tmp/dump_$DATE.bz2
