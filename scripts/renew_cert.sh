#!/bin/bash
DOMAIN=$1

echo "Checking certs for $DOMAIN.thirdcase.de"

if openssl x509 -checkend 604800 -noout -in /etc/letsencrypt/live/$DOMAIN.thirdcase.de/cert.pem && \
		openssl x509 -in /etc/letsencrypt/live/$DOMAIN.thirdcase.de/cert.pem -noout -subject | grep $DOMAIN.thirdcase.de ; then
	echo \"Certificate is good!\" ;
else
	sudo su -c "/etc/init.d/httpd stop && /opt/letsencrypt/letsencrypt-auto renew ; /etc/init.d/httpd start"
	sudo su -c "aws s3 sync /etc/letsencrypt/live/$DOMAIN.thirdcase.de s3://thirdcase-certs/"
fi
