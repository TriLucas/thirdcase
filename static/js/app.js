//-----------------------------------------------
// Copyright 2014 - Tristan Lucas - Alle Rechte vorbehalten
//-----------------------------------------------
var change_counter = 0;

var current_data = null;
function show_results() {
	data = current_data;
	$("#song_yt_suggestions").css('display','block');
	$("#song_yt_suggestions").html('<h4>Auf Youtube gefunden:</h4>');
	for (var idx = 0; idx < data['items'].length; idx++) {
		$("#song_yt_suggestions").append(
			'<a href="https://www.youtube.com/watch?v=' + data['items'][idx]['id']['videoId'] +'" target="_blank">'+
			data['items'][idx]['snippet']['title'].substring(0,50) + '...</a><i onclick="fill_yt_link('+idx+');">THIS ONE</i><br/>'
			);
	}
}

function fill_yt_link(idx) {
	$("#song_yt_suggestions").css('display','none');
	var vid_id = current_data['items'][idx]['id']['videoId'];
	$("#id_video_link").val("https://www.youtube.com/watch?v=" + vid_id);
	$.ajax({
		url:"https://www.googleapis.com/youtube/v3/videos?key=AIzaSyAEJ7HLr45T1ulKgNKBFGmiwOa7h6q1f0E&part=contentDetails&fields=items(contentDetails(duration))&id=" + vid_id,
		context: document.body
	}).success(function(data) {
		$("#id_length").val(data['items'][0]['contentDetails']['duration'].replace("PT","").replace("M",":").replace("S",""));
	});
}

function search() {
	var full_search = '';
	full_search += $("#id_artist").val();
	full_search += ' ';
	full_search += $("#id_title").val();
	$.ajax({
		url: "https://www.googleapis.com/youtube/v3/search?key=AIzaSyAEJ7HLr45T1ulKgNKBFGmiwOa7h6q1f0E&part=snippet&fields=items(id,snippet(title))&q=" + full_search,
		context: document.body
	}).success(function(data, textStatus, jqXHR) {
		current_data = data
		show_results();
	});
}

function check_search() {
	change_counter += 1;
	if (change_counter >= 15) {
		change_counter = 0;
		search();
	}
}

$(document).ready(function() {
	$("#id_artist").keyup(check_search);
	$("#id_artist").change(search);
	$("#id_title").keyup(check_search);
	$("#id_title").change(search);
});
/* EOF */
