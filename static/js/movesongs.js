var act_sethash = null;

var poller_id = null;

function sel_up(event) {
	event.preventDefault();
	event.stopPropagation();
	var id = $(this.parentElement).children('input[type=hidden]').attr('value');
	post_form('?dir=up&song=' + id);	
}

function sel_down(event) {
	event.preventDefault();
	event.stopPropagation();
	var id = $(this.parentElement).children('input[type=hidden]').attr('value');
	post_form('?dir=down&song=' + id);	
}

function sel_right(event) {
	event.preventDefault();
	event.stopPropagation();
	var id = $(this.parentElement).children('input[type=hidden]').attr('value');
	post_form('?dir=right&song=' + id);	
}

function sel_left(event) {
	event.preventDefault();
	event.stopPropagation();
	var id = $(this.parentElement).children('input[type=hidden]').attr('value');
	post_form('?dir=left&song=' + id);	
}

function post_form(params) {
	clearTimeout(poller_id);
	$('.btn_move_up, .btn_move_down, .btn_move_right, .btn_move_left').attr('disabled','disabled');
	$.post(
		$('#ajax_song_form').attr('action') + params,
		$('#ajax_song_form').serialize(),
		function(data,status,xhr) {
			$('#ajax_song_form').replaceWith(data);
			connect();

		});
}

function poll() {
	$.ajax({
		url: $("#tc_ajax_poller")[0].action,
		type: 'get',
		success: function(data) {
			json_o = JSON.parse(data);
			if (json_o['hash'] != act_sethash) {
				$('.btn_move_up, .btn_move_down, .btn_move_right, .btn_move_left').attr('disabled','disabled');
				$('.alert').html(json_o['editor'] + ' hat das Set verändert! Lade die Seite neu!');
			} else {
				poller_id = setTimeout(poll, 1500);
			}
		}
	});
}

function connect() {
	$('.btn_move_up').click(sel_up);
	$('.btn_move_down').click(sel_down);
	$('.btn_move_right').click(sel_right);
	$('.btn_move_left').click(sel_left);

	act_sethash = $('input[name=set_hash]').attr('value');
	poller_id = setTimeout(poll, 2000);
}

$(document).ready(function() {
	connect();
});
/* EOF */
