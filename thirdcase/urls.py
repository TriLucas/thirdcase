from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'', include('apps.listManager.urls')),
    url(r'^', include('apps.songlist.urls')),
    url(r'^profile/', include('apps.thirdcasler.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
